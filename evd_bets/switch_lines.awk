# run with e.g.
# gawk -f switch_lines.awk switched_mamicone.txt

# The following lines need to be switched with the following line in a bunch of files
# 33, 37, 39, 45

BEGIN {
  sw[33] = 34
  sw[37] = 38
  sw[39] = 40
  sw[45] = 46
}

{
	li[NR] = $0
}

END {
	for (i=1; i<=NR; i++) {
		if (i in sw) {
			print li[sw[i]]
			print li[i]
			i++
		}
		else {
			print li[i]
		}
	}
	printf("\n");
}