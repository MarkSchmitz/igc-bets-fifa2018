FROM node:10-slim AS node_bet2018_build-env
LABEL builder="true"
# install all tools needed for project and run npm install
RUN apt-get update && apt-get install -y git build-essential
RUN git clone --depth 1 -b master https://gitlab.com/MarkSchmitz/igc-bets-fifa2018.git fifa2018 \
  && cd fifa2018/ \
  && npm install \
  && npm audit fix

# run from git-root dir
# docker build -f ./Docker/node_bet2018/build-env.Dockerfile -t node_bet2018_build-env --target node_bet2018_build-env .