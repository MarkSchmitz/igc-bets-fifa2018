FROM node:10-slim AS node_bet2018
LABEL builder="false"
# COPY --from=node_bet2018_build-env /fifa2018 /fifa2018

# RUN apt-get update && apt-get install -y git \
#   && cd /fifa2018 \
#   && git pull

# COPY ./Docker/node_bet2018/node_modules.tgz /var/tmp/

RUN wget https://gitlab.com/MarkSchmitz/igc-bets-fifa2018/-/archive/master/igc-bets-fifa2018-master.tar.gz \
  && tar xf igc-bets-fifa2018-master.tar.gz \
  && mv -v igc-bets-fifa2018-master fifa2018 \
  && rm -v igc-bets-fifa2018-master.tar.gz \
  && cd fifa2018 \
  && tar xf /fifa2018/Docker/node_bet2018/node_modules.tgz \
  && rm -v /fifa2018/Docker/node_bet2018/node_modules.tgz

WORKDIR /fifa2018

CMD ["/usr/local/bin/node", "/fifa2018/index.js"]

# run from git-root dir
# docker build -f ./Docker/node_bet2018/node-slim.Dockerfile -t kramred/node_bet2018 --target node_bet2018 .